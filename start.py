import pika
import sys
import json
import requests
import os
from pprint import pprint


rabbi = os.getenv("RABBITMQ")
print("Running with user: %s" % rabbi)

parameters = pika.connection.URLParameters(rabbi)
connection = pika.BlockingConnection(parameters)

channel = connection.channel()

channel.exchange_declare(exchange='microservice_ex', exchange_type='direct')

result = channel.queue_declare(queue='', exclusive=True) 
queue_name = result.method.queue


print('Transport 4x48 is ready to take bookings')


key = os.getenv("ROUTING_KEY")
channel.queue_bind(exchange='microservice_ex', queue=queue_name, routing_key=key)

taxiendpoint = os.getenv("TAXI_ENDPOINT")

channel.exchange_declare(exchange='msres_ex', exchange_type='direct')
channel.queue_declare(queue='msres_queue', exclusive=False)
channel.queue_bind(exchange='msres_ex', queue='')

def callback(ch, method, properties, body):
    
    jsondata = json.loads(body)
    
    datamodel = json.dumps(jsondata)
    print(datamodel)
    response = requests.post(taxiendpoint + '/taxa', headers = {'content-type': 'application/json'}, data=datamodel) 
    
    print(response.status_code) 
   
    if response:
        print('Success')
    else:
        print('An error has occurred') 
    
    
    print(response.content)
    
    
    jdata = json.loads(response.content)
    
    jdata['type'] = 'transport'   

    jdictstr = json.dumps(jdata)
    print(jdictstr)        

    ch.basic_publish(exchange='msres_ex', routing_key='', body=jdictstr)

    
channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)
channel.start_consuming()

